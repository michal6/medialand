<?php

declare(strict_types=1);

namespace App\DataFixtures;

use App\Entity\Role;
use App\Entity\User;
use DateTime;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class UserFixtures extends Fixture
{

    public function load(ObjectManager $manager)
    {
        $user = new User();
        $user->id = 1;
        $user->name = 'MichalG';
        $user->email = 'michal@giergielewicz.pl';
        $user->age = 22;
        $user->role = $manager->find(Role::class, 1);
        $user->isActivated = false;
        $user->loginDate = new DateTime();
        $manager->persist($user);
        $manager->flush();
    }
}