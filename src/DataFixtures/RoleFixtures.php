<?php

namespace App\DataFixtures;

use App\Entity\Role;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class RoleFixtures extends Fixture
{
    public function load(ObjectManager $manager): void
    {
        $manager->persist(Role::create(1, 'User'));
        $manager->persist(Role::create(2, 'Admin'));
        $manager->persist(Role::create(3, 'SuperUser'));

        $manager->flush();
    }
}
