<?php

declare(strict_types=1);

namespace App\Controller;


use Monolog\Logger;
use Psr\Log\LogLevel;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Attribute\Route;

class HomeController extends AbstractController
{

    #[Route('/', 'home')]
    public function home(Logger $logger, Request $request, string $userName = 'no-name'): RedirectResponse
    {
        return $this->redirectToRoute('users');
    }
}