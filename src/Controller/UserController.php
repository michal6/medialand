<?php

declare(strict_types=1);

namespace App\Controller;

use App\Controller\Form\RegisterUserType;
use App\Entity\User;

use App\Repository\UserRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Attribute\Route;
use Symfony\Component\Security\Http\Attribute\IsGranted;


#[IsGranted('ROLE_ADMIN')]
class UserController extends AbstractController
{
    #[Route('/user/register', 'user_register')]
    public function register(Request $request, EntityManagerInterface $em): Response
    {
        $user = new User();
        $form = $this->createForm(RegisterUserType::class, $user);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em->persist($user);
            $em->flush();
            $this->addFlash(
                'success',
                'Your changes were saved!'
            );
            return $this->redirectToRoute('user_register');
        }

        return $this->render(
            'user/register.html.twig',
            ['form' => $form]
        );
    }


    #[Route('/users', 'users')]
    public function users(UserRepository $repository): Response
    {
        $users = $repository->findAll();
        return $this->render(
            'user/users.html.twig',
            ['users' => $users]
        );
    }
}