<?php

declare(strict_types=1);

namespace App\Controller\Form;

use App\Entity\Role;
use App\Repository\RoleRepository;
use DateTime;
use Doctrine\ORM\QueryBuilder;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;

class RegisterUserType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', TextType::class)
            ->add('email', EmailType::class, [
                'attr' => ['placeholder' => 'Enter valid email address'],
            ])
            ->add('age', IntegerType::class)
            ->add(
                'role',
                EntityType::class,
                [
                    'class' => Role::class,
                    'choice_label' => 'name',
                    'query_builder' => function (RoleRepository $repository): QueryBuilder {
                        return $repository
                            ->createQueryBuilder('r')
                            ->where('r.status = :status')
                            ->setParameter(
                            'status',
                            'enabled'
                        );
                    },
                    'placeholder' => 'Select a role',
                ]
            )
            ->add('isActivated', CheckboxType::class, [
                'required' => false
            ])
            ->add('loginDate', DateType::class, [
                'data' => new DateTime()
            ])
            ->add('create', SubmitType::class, ['label' => 'create user']);
    }
}