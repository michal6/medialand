<?php

declare(strict_types=1);

namespace App\Entity;

use App\Repository\UserRepository;
use DateTime;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

#[ORM\Entity(repositoryClass: UserRepository::class)]
class User
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    public ?int $id;

    #[ORM\Column('name', length: 255)]
    #[Assert\Regex('/^[A-Z].*/', message:'Zacznij z Duzej :)')]
    public string $name;

    #[ORM\Column(length: 255)]
    public string $email;

    #[ORM\Column(type: 'integer')]
    #[Assert\LessThan(100)]
    #[Assert\GreaterThan(0)]
    public int $age;

    #[ORM\ManyToOne(Role::class)]
    #[Assert\NotBlank]
    public Role $role;

    #[ORM\Column(type: 'boolean')]
    public bool $isActivated;

    #[ORM\Column(type: 'date')]
    public DateTime $loginDate;

    public function __construct()
    {

    }
}